package lt.codeacademy.interfeisai.task1.interfeses;

import lt.codeacademy.interfeisai.task1.pagalbiniai.Pozicija;

public interface JudetiPirmyn {

	void pirmyn();

	int greitis();
}
