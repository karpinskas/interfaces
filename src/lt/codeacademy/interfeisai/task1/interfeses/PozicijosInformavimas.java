package lt.codeacademy.interfeisai.task1.interfeses;

import lt.codeacademy.interfeisai.task1.enums.Kryptis;
import lt.codeacademy.interfeisai.task1.pagalbiniai.Pozicija;

public interface PozicijosInformavimas {

	Pozicija pozicija();
	Kryptis kryptis();
	
}
