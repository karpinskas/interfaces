package lt.codeacademy.interfeisai.task1;

import lt.codeacademy.interfeisai.task1.aplinka.DegalinesServisas;
import lt.codeacademy.interfeisai.task1.aplinka.PosukiuAtlikimoServisas;
import lt.codeacademy.interfeisai.task1.aplinka.PozisijosKeitimoServeisas;
import lt.codeacademy.interfeisai.task1.transportas.TransportoPriemone;

public class Task1MainClass {

	public static void main(String[] args) {
		new AutoParkasApp().play();
	}

}