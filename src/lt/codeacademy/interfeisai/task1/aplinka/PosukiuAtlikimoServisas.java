package lt.codeacademy.interfeisai.task1.aplinka;

import lt.codeacademy.interfeisai.task1.interfeses.AtlikitPosuki;

public class PosukiuAtlikimoServisas {

	
	
	public void pasisuktiDesines(AtlikitPosuki priemone) {
		priemone.iDesine();
	}

	public void pasisuktiKairen(AtlikitPosuki priemone) {
		priemone.iKaire();
	}
}
